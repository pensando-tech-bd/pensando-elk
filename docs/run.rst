.. _running-pensando-elk:

Start
======================

While the install and setup procedures are done only once, these run instructions can be
run numerous times since you may start and :ref:`stop-pensando-elk` ad-hoc.

.. code-block:: bash